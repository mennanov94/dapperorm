﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Models;

namespace DapperORM.StoredProcedure
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["BookStore"].ConnectionString;
            using (IDbConnection connection = new SqlConnection(connectionString))
            {

                var author = new Author
                {
                    Name = "Test test",
                    BirthDate = new DateTime(1981, 1, 1)
                };

                #region insert using stored procedure
                author.Id = connection
                    .Query<int>("dbo.insert_authors", new { author.Name, author.BirthDate }, commandType: CommandType.StoredProcedure)
                    .First();

                Console.WriteLine("Into table dbo.Author inserter new item with Id = {0}", author.Id);
                #endregion

                #region select using stored procedure
                var selectedValue = connection
                    .Query<Author>("dbo.select_authors", new { author.Id }, commandType: CommandType.StoredProcedure)
                    .First();

                Console.WriteLine("From table dbo.Author selected record with Id = {0} and field values {1}", author.Id, selectedValue);
                #endregion

                #region update using stored procedure 
                connection.Execute(
                    "dbo.update_authors",
                    new { author.Id, BirthDate = new DateTime(2000, 1, 1) },
                    commandType: CommandType.StoredProcedure);

                selectedValue = connection
                    .Query<Author>("dbo.select_authors", new { author.Id }, commandType: CommandType.StoredProcedure)
                    .First();

                Console.WriteLine("Retrieve data after update with Id = {0}, and field values {1}", author.Id, selectedValue);
                #endregion

                #region delete using stored procedure

                Console.WriteLine("Removing record with Id = {0} from table ", author.Id);
                connection.Execute("dbo.delete_authors", new { author.Id }, commandType: CommandType.StoredProcedure);

                #endregion
            }
        }
    }
}
