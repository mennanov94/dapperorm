﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Models;

namespace DapperORM.Query
{
    class Program
    {
        static void Main()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["BookStore"].ConnectionString;

            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                #region select
                Console.WriteLine("\nSingle select from table.");
                string selectQuery = "select Name, BirthDate from dbo.Author";
                var authors = connection.Query<Author>(selectQuery);

                foreach (var author in authors)
                {
                    Console.WriteLine(author.ToString());
                }
                #endregion

                string selectQueryWithJoinStatement =
                    "select a.Id, a.Name, a.BirthDate, b.Id, b.Title, b.Price from dbo.Author as a inner join dbo.Book as b on a.Id = b.AuthorId";

                #region create duplicates
                Console.WriteLine("\nSelect using join from Author and Books tables.\nCreates duplicate objects.");
                var authorsFromJoin = connection.Query<Author, Book, Author>(selectQueryWithJoinStatement, (author, book) =>
                {
                    author.Books = author.Books ?? new List<Book>();
                    author.Books.Add(book);
                    return author;
                });

                foreach (var author in authorsFromJoin)
                {
                    Console.WriteLine($"{author}\n{author.GetBookList()}");
                }
                #endregion

                #region do not create pulicates
                Console.WriteLine("\nSelect using join from Author and Books tables.\nDo not creates duplicate objects.");
                var uniqAuthorAndBook = new List<Author>();
                connection.Query<Author, Book, Author>(selectQueryWithJoinStatement, (author, book) =>
                {
                    var authorFromList = uniqAuthorAndBook.FirstOrDefault(listItem => listItem.Id == author.Id);
                    if (authorFromList == null)
                    {
                        uniqAuthorAndBook.Add(author);
                        authorFromList = author;
                    }

                    authorFromList.Books = authorFromList.Books ?? new List<Book>();
                    authorFromList.Books.Add(book);

                    return null;
                });

                foreach (var author in uniqAuthorAndBook)
                {
                    Console.WriteLine($"{author}\n{author.GetBookList()}");
                }
                #endregion

                #region multiple

                Console.WriteLine("Query multiple. From Author and Genre table.");
                string selectMultiple = "select Id, Name from dbo.Author; select Id, Name from dbo.Genre;";

                using (var reader = connection.QueryMultiple(selectMultiple))
                {
                    var authorsFromQueryMultiple = reader.Read<Author>();
                    var genresFromQueryMultiple = reader.Read<Genre>();

                    foreach (var author in authorsFromQueryMultiple)
                    {
                        Console.WriteLine(author);
                    }

                    foreach (var genre in genresFromQueryMultiple)
                    {
                        Console.WriteLine(genre);
                    }
                }

                #endregion
            }
        }
    }
}
