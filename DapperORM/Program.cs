﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using Models;

namespace DapperORM
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["BookStore"].ConnectionString;

            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                var authors = new List<Author>
                {
                    new Author {Name = "Martin Fowler", BirthDate = new DateTime(1963, 1, 1)},
                    new Author {Name = "Mark Seemann"}
                };


                var insertQuery = "insert into dbo.Author(Name, BirthDate) values(@Name, @BirthDate)";
                var insertedRowCount = connection.Execute(insertQuery, authors);
                Console.WriteLine($"In table Author {insertedRowCount} were inserted.\n");



                var selectCountQuery = "select count(*) from dbo.Author";
                var count = connection.ExecuteScalar<int>(selectCountQuery);
                Console.WriteLine($"In table Author {count} elements is exists.\n");



                var updateQuery = "update dbo.Author set BirthDate = @BirthDate where Name = @Name";
                var updateRowCount = connection.Execute(updateQuery, new { BirthDate = new DateTime(2000, 1, 1), Name = "Mark Seemann" });
                Console.WriteLine($"In table Author {updateRowCount} is updated.\n");


                var selectQuery = "select * from dbo.Author";
                var entitiesFromDb = connection.Query<Author>(selectQuery);

                foreach (var author in entitiesFromDb)
                {
                    Console.WriteLine(author.ToString());
                }
                Console.WriteLine();


                var deleteQuery = "delete from dbo.Author where Name = @Name";
                var deletedRowCount = connection.Execute(deleteQuery, authors);
                Console.WriteLine($"From table Author {deletedRowCount} was deleted.");
            }
        }
    }
}
