﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class Author
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? BirthDate { get; set; }
        public List<Book> Books { get; set; }

        public override string ToString()
        {
            return $"Name: {Name} - BirthDate: {BirthDate?.ToString("yyyy MMMM dd")}";
        }

        public string GetBookList()
        {
            var stringBuilder = new StringBuilder();

            foreach (var book in Books)
            {
                stringBuilder.AppendFormat("\t{0}", book);
            }

            return stringBuilder.ToString();
        }
    }
}