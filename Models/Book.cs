﻿namespace Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public Author Author { get; set; }
        public int AuthorId { get; set; }

        public override string ToString()
        {
            return $"Title: {Title}\tPrice: {Price:C}\n";
        }
    }
}