﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Models;

namespace DapperORM.Repository.Repository
{
    public class BookRepository : IRepository<Book>
    {
        private readonly string _connectionString;

        public BookRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public Book Add(Book book)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                book.Id = connection.QueryFirst<int>(
                    "insert into dbo.Book(Title, Price, AuthorId) values(@Title, @Price, @AuthorId);select scope_identity()",
                    new { book.Title, book.Price, book.AuthorId }
                    );
                return book;
            }
        }

        public void Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Execute("delete from dbo.Book where Id = @Id", new { Id = id });
            }
        }

        public Book Get(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<Book, Author, Book>(
                        "select b.Id, b.Title, b.Price, a.Id, a.Name from dbo.Book as b inner join Author as a on b.AuthorId = a.Id where b.Id = @Id",
                        (book, author) => { book.Author = author; return book; },
                        new { Id = id })
                    .First();
            }
        }

        public List<Book> Get()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Book>("select Id, Title, Price from dbo.Book").ToList();
            }
        }

        public void Update(Book entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Execute(
                    "update dbo.Book set Title = isnull(@Title, Title), Price = isnull(@Price, Price) where Id = @Id",
                    new {entity.Title, entity.Price, entity.Id});
            }
        }
    }
}
