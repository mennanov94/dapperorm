﻿using System.Collections.Generic;

namespace DapperORM.Repository.Repository
{
    public interface IRepository<T>
    {
        T Add(T book);

        void Delete(int id);

        T Get(int id);

        List<T> Get();

        void Update(T entity);
    }
}
