﻿using System;
using System.Configuration;
using DapperORM.Repository.Repository;
using Models;

namespace DapperORM.Repository
{
    class Program
    {
        static void Main()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["BookStore"].ConnectionString;
            IRepository<Book> repository = new BookRepository(connectionString);

            var book = new Book
            {
                Title = "Some Book",
                Price = 10m,
                AuthorId = 1
            };

            #region insert using repository
            book = repository.Add(book);
            Console.WriteLine("Into table dbo.Author inserted new record with Id = {0}", book.Id);
            #endregion

            #region select using repository
            var selectedBook = repository.Get(book.Id);
            Console.WriteLine("Book from database\n" +
                              $"\tTitle: {selectedBook.Title}\n" +
                              $"\tPrice: {selectedBook.Price:C}\n" +
                              $"\tAuthor: {selectedBook.Author.Name}\n");
            #endregion

            #region select many using repository
            Console.WriteLine("All Books from database:");
            var booksList = repository.Get();
            foreach (var item in booksList)
            {
                Console.WriteLine($"Id = {item.Id},\tPrice = {item.Price},\tTitle = {item.Title}");
            }
            #endregion

            #region update using repository

            repository.Update(new Book { Title = null, Price = 25m, Id = book.Id });
            var updatedBook = repository.Get(book.Id);
            Console.WriteLine($"Update book. Set new price.\nId = {updatedBook.Id}, Title = {updatedBook.Title}, Price = {updatedBook.Price}");

            #endregion

            #region delete using repository
            Console.WriteLine("Removing record from database with id = {0}", book.Id);
            repository.Delete(book.Id);
            #endregion
        }
    }
}
