﻿create table dbo.Author
	(
		Id int not null primary key identity(1,1),
		Name nvarchar(255) not null,
		BirthDate date
	)
