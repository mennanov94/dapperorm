﻿create table dbo.Genre
	(
		Id int not null primary key identity(1,1),
		Name nvarchar(100) not null
	)
