﻿create table dbo.Book
	(
		Id int not null primary key identity(1,1),
		Title nvarchar(100) not null,
		Price money not null,
		AuthorId int not null foreign key references Author(Id) 
	)
