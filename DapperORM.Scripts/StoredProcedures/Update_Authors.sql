﻿use BookStore
go
create procedure dbo.update_authors
	@Id int,
	@Name nvarchar(255) = null,
	@BirthDate date = null
as begin 
	update dbo.Author set Name = ISNULL(@Name, Name), BirthDate = ISNULL(@BirthDate, BirthDate) where Id = @Id
end 
go

--execute dbo.update_authors @Id = 4, @Name = 'Super Secret'
--execute dbo.update_authors @Id = 3, @BirthDate = '2000-01-01'
