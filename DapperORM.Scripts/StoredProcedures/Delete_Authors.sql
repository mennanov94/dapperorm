﻿use BookStore
go 
create procedure dbo.delete_authors
	@Id int
as begin
	delete from dbo.Author where Id = @Id
end
go

--execute dbo.delete_authors @Id = 7
