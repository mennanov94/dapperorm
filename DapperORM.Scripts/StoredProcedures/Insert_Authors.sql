﻿use BookStore
go
create procedure dbo.insert_authors
	@Name nvarchar(255),
	@BirthDate date = null
as begin
	insert into dbo.Author(Name, BirthDate) values(@Name, @BirthDate)

	select SCOPE_IDENTITY()
end
go

--execute dbo.insert_authors 'Test', '1970-01-01'
