﻿use BookStore
go
create procedure dbo.select_authors
	@Id int
as begin
	set nocount on
	select Id, Name, BirthDate from dbo.Author where Id = @Id
end
go

--execute dbo.select_authors @Id=1
