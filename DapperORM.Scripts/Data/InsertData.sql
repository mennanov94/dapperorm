﻿declare @lastInsertedAuthorId int

insert into dbo.Author(Name) values('Jon Skeet')
set @lastInsertedAuthorId = SCOPE_IDENTITY()

insert into dbo.Book(Title, Price, AuthorId) values('C# in Depth', 49.9, @lastInsertedAuthorId),
												   ('Groovy in Action', 15, @lastInsertedAuthorId)

insert into dbo.Author(Name) values('K. Scott Allen')
set @lastInsertedAuthorId = SCOPE_IDENTITY()
insert into dbo.Book(Title, Price, AuthorId) values('What Every Web Developer Should Know About HTTP', 3, @lastInsertedAuthorId),
												   ('What Every JavaScript Developer Should Know About ECMAScript 2015', 3, @lastInsertedAuthorId)

insert into dbo.Genre values('Novel'),('Scientific')
